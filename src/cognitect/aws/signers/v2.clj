;; Copyright (c) Cognitect, Inc.
;; All rights reserved.

(ns ^:skip-wiki cognitect.aws.signers.v2
  "Impl, don't call directly."
  (:require [clojure.string :as str]
            [cognitect.aws.client :as client]
            [cognitect.aws.service :as service]
            [cognitect.aws.util :as util])
  (:import [java.net URI]
           [java.net URLDecoder]))

(set! *warn-on-reflection* true)

(defn uri-decode [^String s]
  (URLDecoder/decode s))

(defn uri-encode
  "Escape (%XX) special characters in the string `s`.

  Letters, digits, and the characters `_-~.` are never encoded.

  The optional `extra-chars` specifies extra characters to not encode."
  ([^String s]
   (when s
     (uri-encode s "")))
  ([^String s extra-chars]
   (when s
     (let [safe-chars (->> extra-chars
                           (into #{\_ \- \~ \.})
                           (into #{} (map int)))
           builder    (StringBuilder.)]
       (doseq [b (.getBytes s "UTF-8")]
         (.append builder
                  (if (or (Character/isLetterOrDigit ^int b)
                          (contains? safe-chars b))
                    (char b)
                    (format "%%%02X" b))))
       (.toString builder)))))

(defn- canonical-method
  [{:keys [request-method]}]
  (-> request-method name str/upper-case))

(defn- canonical-uri
  [{:keys [uri]}]
  (let [encoded-path (-> uri
                         (str/replace #"//+" "/") ; (URI.) throws Exception on '//'.
                         (str/replace #"\s" "%20"); (URI.) throws Exception on space.
                         (URI.)
                         (.normalize)
                         (.getPath)
                         (uri-encode "/"))]
    (if (.isEmpty ^String encoded-path)
      "/"
      encoded-path)))

(defn canonical-request
  [{:keys [server-name] :as request} canonical-query-string]
  (str/join "\n" [(canonical-method request)
                  server-name
                  (canonical-uri request)
                  canonical-query-string]))

(defn signature [secret-access-key canonical-request]
  (uri-encode
   (util/base64-encode
    (util/hmac-sha-256 (.getBytes ^String secret-access-key)
                       canonical-request))))

;; TODO: likely a safer way to do this
;;  i.e. is the body ever NOT a HeapByteBuffer?? --mdp
(defn body-as-string [{:keys [body] :as http-request}]
  (String. (.array ^java.nio.HeapByteBuffer body)))

(defn timestamp []
  (str (java.time.Instant/now)))

(defn params [{:keys [:aws/access-key-id :aws/session-token] :as credentials} http-request]
  (merge
   (->> (str/split (body-as-string http-request) #"[&]")
        (mapv (fn [p] (str/split p #"[=]" 2)))
        (mapv (fn [[k v]] [(uri-decode k) (uri-decode v)]))
        (into {}))
   {"AWSAccessKeyId" access-key-id
    "Timestamp" (timestamp)
    "SignatureMethod" "HmacSHA256"
    "SignatureVersion" "2"}
   (when session-token
     {"SecurityToken" session-token})))

(defn canonical-query-string [params]
  (let [params (into (sorted-map) params)]
    (str/join
     "&"
     (reduce
      (fn [coll [k v]]
        (conj coll (format "%s=%s"
                           (uri-encode (str k))
                           (uri-encode (str v)))))
      []
      params))))

(defn v2-sign-http-request
  [service region credentials http-request]
  (let [{:keys [:aws/secret-access-key]} credentials
        params (params credentials http-request)
        canonical-query-string (canonical-query-string params)
        canonical-request (canonical-request http-request canonical-query-string)
        signature (signature secret-access-key canonical-request)]
    (assoc http-request
           :uri (canonical-uri http-request)
           :body (.encode
                  java.nio.charset.StandardCharsets/UTF_8
                  (format "%s&Signature=%s" canonical-query-string signature)))))

(defmethod client/sign-http-request "v2"
  [service region credentials http-request]
  (v2-sign-http-request service region credentials http-request))
