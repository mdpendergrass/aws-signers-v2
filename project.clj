(defproject com.cognitect.aws/signers-v2 "0.0.3-SNAPSHOT"

  :description "v2 signatures for com.cognitect.aws"

  :url "https://bitbucket.org/mdpendergrass/aws-signers-v2.git"

  :scm {:url "git@bitbucket.org:mdpendergrass/aws-signers-v2"}

  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [
                 [com.cognitect.aws/api "LATEST" :scope "provided"]
                 [com.cognitect.aws/endpoints "LATEST" :scope "provided"]
                 ]

  :deploy-repositories [["snapshots"
                         {:url "https://clojars.org/repo"
                          :sign-releases false
                          :creds :gpg}]
                        ["releases"
                         {:url "https://clojars.org/repo"
                          :sign-releases false
                          :creds :gpg}]]

  :release-tasks [["vcs" "assert-committed"]
                  ["change" "version" "leiningen.release/bump-version" "release"]
                  ["vcs" "commit"]
                  ["vcs" "tag" "--no-sign"]
                  ["deploy"]
                  ["change" "version" "leiningen.release/bump-version"]
                  ["vcs" "commit"]
                  ["vcs" "push"]])
